package com.eworldes.filenet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.Subject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.eworldes.filenet.configs.ApplicationConstants;
import com.eworldes.filenet.models.CEConnection;
import com.filenet.api.collection.ObjectStoreSet;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;

@SpringBootApplication
public class FilenetApplication extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(FilenetApplication.class);
	  }

	public static void main(String[] args) {
		SpringApplication.run(FilenetApplication.class, args);
		System.out.println("Hi there, I'm up!!!");
		
//		-Dwasp.location=C:\Users\eworld\Downloads\JavaCEWSclient\wsi
//		-Djava.security.auth.login.config=C:\Users\eworld\Downloads\JavaCEWSclient\jaas.conf.WSI
	//////////////////////	
//		try {
			
//			 Connection connection = Factory.Connection.getConnection(ApplicationConstants.CE_URI_WSI); 
//			 Subject sub = UserContext.createSubject(connection, ApplicationConstants.USER_NAME, ApplicationConstants.PASSWORD, ApplicationConstants.STANZA_WSI); 
//			 UserContext uc = UserContext.get(); uc.pushSubject(sub);
//			 Domain domain = Factory.Domain.fetchInstance(connection, null, null);
//			 String domainName = domain.get_Name();
//			 System.out.println("Domain Name :: "+domainName);
//			 ObjectStoreSet objectStoreSet = domain.get_ObjectStores();
//				
//			 List<String> objectStoreNames = new ArrayList<>();
//			 Iterator it = objectStoreSet.iterator();
//			 while(it.hasNext()){
//    			ObjectStore os = (ObjectStore) it.next();
//    			objectStoreNames.add(os.get_DisplayName());
//			 }
//			 System.out.println("ObjectStore Names :: "+objectStoreNames.toString());
	////////////////////////		
//			CEConnection ceConn = new CEConnection();
//			ceConn.establishConnection(ApplicationConstants.USER_NAME, ApplicationConstants.PASSWORD, ApplicationConstants.STANZA_WSI, ApplicationConstants.CE_URI_WSI)
//			Domain domain = ceConn.fetchDomain();
//			String domainName = domain.get_Name();
//			System.out.println("Domain Name :: "+domainName);
//    		System.out.println("ObjectStore Names :: "+ceConn.getOSNames().toString());
//		}catch(Exception ex) {
//			System.out.println(ex.getMessage());
//		}	
		
	}

}
