package com.eworldes.filenet.models;

public class GetDocumentRequest {
	private String objectStore;
	private String documentId;
	private String documentPath;
	private boolean includeContent;
	private String folderPath;
	private boolean includeACL;
	
	public String getObjectStore() {
		return objectStore;
	}
	public void setObjectStore(String objectStore) {
		this.objectStore = objectStore;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public String getDocumentPath() {
		return documentPath;
	}
	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}
	public boolean isIncludeContent() {
		return includeContent;
	}
	public void setIncludeContent(boolean includeContent) {
		this.includeContent = includeContent;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public boolean isIncludeACL() {
		return includeACL;
	}
	public void setIncludeACL(boolean includeACL) {
		this.includeACL = includeACL;
	}	
}
 