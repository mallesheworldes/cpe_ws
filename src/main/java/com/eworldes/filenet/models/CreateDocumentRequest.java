package com.eworldes.filenet.models;

import org.springframework.web.multipart.MultipartFile;

public class CreateDocumentRequest {
	private String documentTitle;
	private String objectStore;
	private boolean includeContent;
	private String filePath;
	private MultipartFile documentFile;
	private String mimeType;
	private String documentClass;
	private boolean fileInFolder;
	private String folderPath;
	private boolean checkIn;
	
	public String getDocumentTitle() {
		return documentTitle;
	}
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}
	public String getObjectStore() {
		return objectStore;
	}
	public void setObjectStore(String objectStore) {
		this.objectStore = objectStore;
	}
	public boolean isIncludeContent() {
		return includeContent;
	}
	public void setIncludeContent(boolean includeContent) {
		this.includeContent = includeContent;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public MultipartFile getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(MultipartFile documentFile) {
		this.documentFile = documentFile;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getDocumentClass() {
		return documentClass;
	}
	public void setDocumentClass(String documentClass) {
		this.documentClass = documentClass;
	}
	public boolean isFileInFolder() {
		return fileInFolder;
	}
	public void setFileInFolder(boolean fileInFolder) {
		this.fileInFolder = fileInFolder;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public boolean isCheckIn() {
		return checkIn;
	}
	public void setCheckIn(boolean checkIn) {
		this.checkIn = checkIn;
	}
}
