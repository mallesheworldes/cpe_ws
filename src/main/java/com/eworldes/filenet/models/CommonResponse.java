package com.eworldes.filenet.models;

public class CommonResponse {
	private Object response;
	private String message;
	
	public CommonResponse(Object response, String message) {
		super();
		this.response = response;
		this.message = message;
	}
	
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
