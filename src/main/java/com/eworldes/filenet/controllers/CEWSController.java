package com.eworldes.filenet.controllers;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.Subject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eworldes.filenet.configs.ApplicationConstants;
import com.eworldes.filenet.models.CEConnection;
import com.eworldes.filenet.models.CommonResponse;
import com.eworldes.filenet.models.CreateDocumentRequest;
import com.eworldes.filenet.models.GetDocumentRequest;
import com.eworldes.filenet.utils.CEUtil;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;


@Controller
@RequestMapping("/cews")
public class CEWSController {
	CEConnection ceConn = null;
//	UserContext uc;
//	Connection con;
	
	@GetMapping("/home")
	public String welcome() {
	    return "home";
	}
	
	@GetMapping("/getCreateDoc")
	public String getCreateDoc(@ModelAttribute("document") CreateDocumentRequest docRequest) {
	    return "createDoc";
	}
	
	@GetMapping("/getFetchDoc")
	public String getFetchDoc(@ModelAttribute("document") GetDocumentRequest docRequest) {
	    return "fetchDoc";
	}
	
	@GetMapping("/createCEConnection")
	public String createCEConnection() {
		try {
			ceConn = CEConnection.getInstace();
			ceConn.establishConnection(ApplicationConstants.USER_NAME, ApplicationConstants.PASSWORD, ApplicationConstants.STANZA_WSI, ApplicationConstants.CE_URI_WSI);
			return "Connection successful to domain : "+ceConn.getDomainName();			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return "Connection failed";
	}
	
	@GetMapping("/objectStores")
	public String getObjectStoreNames() {
		if(ceConn == null)
			return "InValid/UnAvailable Connection";
		return ceConn.getOSNames().toString();
	}
	
	
	@PostMapping("/createDocument")
//	public ResponseEntity<CommonResponse> createDocument(@ModelAttribute("document") CreateDocumentRequest docRequest) {
	public String createDocument(@ModelAttribute("document") CreateDocumentRequest docRequest, ModelMap modelMap) {
		Connection con = Factory.Connection.getConnection(ApplicationConstants.CE_URI_WSI); 
		Subject sub = UserContext.createSubject(con, ApplicationConstants.USER_NAME, ApplicationConstants.PASSWORD, ApplicationConstants.STANZA_WSI); 
		UserContext uc = UserContext.get(); 
		uc.pushSubject(sub);
		try {
//			connectionCreation();
			Document doc;
			File file = new File(docRequest.getFilePath());
//			ObjectStore objectStore = ceConn.fetchOS(docRequest.getObjectStore());
			ObjectStore objectStore = Factory.ObjectStore.fetchInstance(Factory.Domain.fetchInstance(con, null, null), docRequest.getObjectStore(), null);
			if(docRequest.isIncludeContent()) {
				doc = CEUtil.createDocWithContent(file, docRequest.getMimeType(), objectStore, docRequest.getDocumentTitle(), docRequest.getDocumentClass());
			}else {
				doc = CEUtil.createDocNoContent(docRequest.getMimeType(), objectStore, docRequest.getDocumentTitle(), docRequest.getDocumentClass());
			}
			doc.save(RefreshMode.REFRESH);
			if(docRequest.isFileInFolder()) {
				ReferentialContainmentRelationship rcr = CEUtil.fileObject(objectStore, doc, docRequest.getFolderPath());
				rcr.save(RefreshMode.REFRESH);
			}
			if(docRequest.isCheckIn()) {
				CEUtil.checkinDoc(doc);
			}
			
			HashMap tempMap = CEUtil.getContainableObjectProperties(doc);
			modelMap.addAllAttributes(tempMap);
			
//			return new ResponseEntity<CommonResponse>(new CommonResponse(CEUtil.getContainableObjectProperties(doc), "Document Created!!"), HttpStatus.OK);
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
//			return new ResponseEntity<CommonResponse>(new CommonResponse(null, ""), HttpStatus.NOT_IMPLEMENTED);
		}finally {
			uc.popSubject();
		}
		return "docResult";
	}
	
	@PostMapping("/getDocument")
//	public ResponseEntity<CommonResponse> getDocument(@RequestBody GetDocumentRequest docRequest) {
	public String getDocument(@ModelAttribute("document") GetDocumentRequest docRequest, ModelMap modelMap) {	
		Connection con = Factory.Connection.getConnection(ApplicationConstants.CE_URI_WSI); 
		Subject sub = UserContext.createSubject(con, ApplicationConstants.USER_NAME, ApplicationConstants.PASSWORD, ApplicationConstants.STANZA_WSI); 
		UserContext uc = UserContext.get(); 
		uc.pushSubject(sub);
		try {
//			connectionCreation();
//			ceConn = CEConnection.getInstace();
			 ObjectStore objectStore = Factory.ObjectStore.fetchInstance(Factory.Domain.fetchInstance(con, null, null), docRequest.getObjectStore(), null);
			 
			 
			Document doc = null;
//			ObjectStore objectStore = ceConn.fetchOS(docRequest.getObjectStore());
			
			if(docRequest.getDocumentId() != null && !docRequest.getDocumentId().isEmpty()) {
				doc = CEUtil.fetchDocById(objectStore, docRequest.getDocumentId());
			}else if(docRequest.getDocumentPath() != null) {
				doc = CEUtil.fetchDocByPath(objectStore, docRequest.getDocumentPath());
			}
			
			Map resultMap = CEUtil.getContainableObjectProperties(doc);			
			if(docRequest.isIncludeContent()) {
				CEUtil.writeDocContentToFile(doc,docRequest.getFolderPath());
				resultMap.put("contentCreated", true);
			}
			
			if(docRequest.isIncludeACL()) {
				resultMap.put("permissions", doc.get_Permissions().toString());
			}
			
			modelMap.addAllAttributes(resultMap);
//			return new ResponseEntity<CommonResponse>(new CommonResponse(resultMap, "Success"), HttpStatus.OK);
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
//			return new ResponseEntity<CommonResponse>(new CommonResponse(null, "Fail"), HttpStatus.NOT_IMPLEMENTED);
		}finally {
			uc.popSubject();
		}
		return "docResult";
	}
	
//	public void connectionCreation() {
//		Connection con = Factory.Connection.getConnection(ApplicationConstants.CE_URI_WSI); 
//		Subject sub = UserContext.createSubject(con, ApplicationConstants.USER_NAME, ApplicationConstants.PASSWORD, ApplicationConstants.STANZA_WSI); 
//		uc = UserContext.get(); 
//		uc.pushSubject(sub);
//	}
}
