<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>FileNet CEWS</title>
</head>
<body>
  <form:form action="/cews/getDocument" modelAttribute="document" method="post">
    <table>
      <tr>
        <td>
          <form:label path="objectStore">Object Store</form:label>
        </td>
        <td>
          <form:select path="objectStore" id="objectStore" align="left">
          	<form:option value="P8ObjectStore">P8ObjectStore</form:option>
          </form:select>
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="documentId">Document ID</form:label>
        </td>
        <td>
          <form:input path="documentId" id="documentId" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="documentPath">Document Path</form:label>
        </td>
        <td>
          <form:input path="documentPath" id="documentPath" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="includeContent">Include Content?</form:label>
        </td>
        <td>
          <form:checkbox path="includeContent" id="includeContent" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="folderPath">Folder Path</form:label>
        </td>
        <td>
          <form:input path="folderPath" id="folderPath" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="includeACL">Include ACL?</form:label>
        </td>
        <td>
          <form:checkbox path="includeACL" id="includeACL" />
        </td>
      </tr>
      <tr>
        <td><input type="submit" value="Submit"></td>
      </tr>
    </table>
  </form:form>
</body>
</html>