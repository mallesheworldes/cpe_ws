<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>FileNet CEWS</title>
</head>
<body>
	<h2 align="left">Create Document</h2>
  <!-- Values entered here are bound to the properties of 
      user object assigned here to modelAttribute -->
  <form:form action="/cews/createDocument" modelAttribute="document" method="post" align="center">
    <table>
      <tr>
        <td>
          <form:label path="documentTitle">Document Title</form:label>
        </td>
        <td>
          <form:input path="documentTitle" id="documentTitle" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="objectStore">Object Store</form:label>
        </td>
        <td>
          <form:select path="objectStore" id="objectStore" align="left">
          	<form:option value="P8ObjectStore">P8ObjectStore</form:option>
          </form:select>
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="includeContent">Include Content?</form:label>
        </td>
        <td>
          <form:checkbox path="includeContent" id="includeContent" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="filePath">filePath</form:label>
        </td>
        <td>
          <form:input path="filePath" id="filePath" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="mimeType">Mime Type</form:label>
        </td>
        <td>
          <form:input path="mimeType" id="mimeType" />
        </td>
      </tr>
      <tr>
        <td>
          <form:label path="documentClass">documentClass</form:label>
        </td>
        <td>
          <form:input path="documentClass" id="documentClass" />
        </td>
      </tr>
      
      
      <tr>
        <td>
          <form:label path="fileInFolder">file In Folder?</form:label>
        </td>
        <td>
          <form:checkbox path="fileInFolder" id="fileInFolder" />
        </td>
      </tr>
      <%-- <%
      	boolean fileInFolderB = Boolean.parseBoolean(request.getAttribute("fileInFolder").toString());
      	
      	if(fileInFolderB){
      %> --%>
	      <tr>
	        <td is="">
	          <form:label path="folderPath">Folder Path</form:label>
	        </td>
	        <td>
	          <form:input path="folderPath" id="folderPath" />
	        </td>
	      </tr>
      		
      <%-- <%}%> --%>
     
      <tr>
        <td>
          <form:label path="checkIn">checkIn?</form:label>
        </td>
        <td>
          <form:checkbox path="checkIn" id="checkIn" align="left"/>
        </td>
      </tr>
      <tr>
        <td><input type="submit" value="Submit"></td>
      </tr>
    </table>
  </form:form>
</body>
</html>