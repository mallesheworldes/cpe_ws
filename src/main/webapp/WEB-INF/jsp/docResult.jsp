<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>FileNet CEWS</title>
</head>
<body>
	<h2 align="left">Document Details</h2>
    <table>
    	<tr>
            <td>ID :</td>
            <td>${Id}</td>
        </tr>
        <tr>
            <td>Name :</td>
            <td>${Name}</td>
        </tr>
        <tr>
            <td>Owner :</td>
            <td>${Owner}</td>
        </tr>
        <tr>
            <td>Creator :</td>
            <td>${Creator}</td>
        </tr>
       <%--  <tr>
            <td>Date Created :</td>
            <td>${Date Created}</td>
        </tr>
        <tr>
            <td>Content Created? :</td>
            <td>${contentCreated}</td>
        </tr> --%>
        <tr>
            <td>Permissions :</td>
            <td>${permissions}</td>
        </tr>
    </table>
    
    
    <br><br>
    <a href="/cews/home">Goto Home</a>
</body>
</html>